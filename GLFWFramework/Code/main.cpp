#include "MyScene.h"

int main(int argc, char* argv[])
{
	Scene *scene = nullptr;
	scene = new MyScene(argc, argv, "LearnOpenGL", 800, 600);

	scene->Run();

	delete scene;
	return 0;
}