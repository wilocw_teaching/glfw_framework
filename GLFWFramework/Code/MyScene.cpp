#include "MyScene.h"

#include <ctime>


MyScene::MyScene(int argc, char** argv, const char *title, const int& windowWidth, const int& windowHeight)
	: Scene(argc, argv, title, windowWidth, windowHeight)
{
	// don't construct/call opengl commands here
	srand(static_cast<unsigned int>(time(nullptr)));
}

void MyScene::Initialise()
{	
	
}