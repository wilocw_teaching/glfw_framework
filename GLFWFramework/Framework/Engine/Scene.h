#pragma once

#include "Engine.h"

#include <vector>

#include "DisplayableObject.h"
#include "Animation.h"
#include "Input.h"
#include "LightSource.h"
#include "Texture.h"
#include "Camera.h"

class DisplayableObject;
class LightSource;
class Camera;

class Scene :
	public Engine
{
public:
	
	Scene(int argc, char** argv, const char* title, const int& windowWidth, const int& windowHeight);
	
	virtual ~Scene();

	static GLuint GetTexture(std::string filename);
	
	static Camera* GetCamera();

	static GLuint GetNumLights();

	static Light** GetLights();

protected:

	virtual void Initialise();

	void Draw();

	void Update(const GLfloat& deltaTime);

	void AddObjectToScene(DisplayableObject* obj);

	void AddLightSource(LightSource* lsrc);

	void HandleKey(int key, int action, int mods);

private:

	std::vector<DisplayableObject*> _objects;

	static Texture _textureLoader;
	
	static std::vector<Light*> _lights;

	static Camera _camera;

};

