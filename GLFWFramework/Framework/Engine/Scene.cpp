#include "Scene.h"

Texture Scene::_textureLoader = Texture();
Camera Scene::_camera = Camera();

std::vector<Light*> Scene::_lights = std::vector<Light*>();

Scene::Scene(int argc, char** argv, const char* title, const int& windowWidth, const int& windowHeight)
	: Engine(argc, argv, title, windowWidth, windowHeight)
{
	
}


Scene::~Scene()
{
	for (DisplayableObject* obj : _objects)
	{
		if (obj != nullptr)
			delete obj;
		obj = nullptr;
	}
}

void Scene::Initialise()
{

}

void Scene::Draw()
{
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (DisplayableObject* obj : _objects)
		obj->Display();

}

void Scene::Update(const GLfloat& deltaTime)
{
	Animation* ani_obj;

	_camera.Update(deltaTime);

	for (DisplayableObject* obj : _objects)
	{
		ani_obj = dynamic_cast<Animation*>(obj);
		if (ani_obj != nullptr)
			ani_obj->Update(deltaTime);
	}
}

void Scene::AddObjectToScene(DisplayableObject* obj)
{
	_objects.push_back(obj);
}

void Scene::AddLightSource(LightSource* lsrc)
{
	_lights.push_back(lsrc->GetLight());
}

GLuint Scene::GetTexture(std::string filename)
{
	return _textureLoader.GetTexture(filename);
}

Camera* Scene::GetCamera()
{
	return &_camera;
}

GLuint Scene::GetNumLights()
{
	return _lights.size();
}

Light** Scene::GetLights()
{
	return &_lights[0];
}

void Scene::HandleKey(int key, int action, int mods)
{
	Input* input_obj = nullptr;

	for (DisplayableObject* obj : _objects)
	{
		input_obj = dynamic_cast<Input*>(obj);
		if (input_obj != nullptr)
			input_obj->HandleKey(key, action, mods);
	}
}