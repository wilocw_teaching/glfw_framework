#pragma once

#include "Scene.h"
#include "Shader.h"

struct Material
{
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	GLfloat shininess;
};

class DisplayableObject
{
public:
	DisplayableObject(){ }
	virtual ~DisplayableObject(){}

	virtual void Display() = 0;

protected:
	Shader* _program;
	Material _material;
};