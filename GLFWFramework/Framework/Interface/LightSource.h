#pragma once

#include "Scene.h"
#include "Shader.h"

// Light properties
struct Light
{
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::vec4 position;
};

class LightSource
{
public:
	LightSource(){}

	virtual ~LightSource(){}

	inline Light* GetLight() { return &_light; }
	
protected:

	virtual void InitialiseLightSource() = 0;
	
	Light _light;
};