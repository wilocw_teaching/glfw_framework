#pragma once

class Animation
{
public:
	Animation() {}
	virtual ~Animation() {}

	virtual void Update(const GLfloat& deltaTime) = 0;
};