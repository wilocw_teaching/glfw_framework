#include "Texture.h"

#include <SOIL/SOIL.h>

#include <iostream>

using namespace std;

Texture::Texture() : _format(GL_RGBA)
{
}


Texture::~Texture()
{
	GLuint tex;
	for (map<string, GLuint>::iterator it = _textures.begin(); it != _textures.end(); ++it)
	{
		tex = it->second;
		glDeleteTextures(1, &tex);
	}
}

GLuint Texture::GetTexture(string filename)
{
	if (_textures.find(filename) != _textures.end())
		return _textures[filename];

	GLsizei width, height;
	unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, 0, _format == GL_RGBA ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);

	if (image == NULL)
	{
		std::cout << "Error, unable to read image " << filename << ": " << SOIL_last_result() << std::endl;
		return NULL;
	}
	
	GLuint texture;
	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, _format, width, height, 0, _format, GL_UNSIGNED_BYTE, static_cast<GLvoid*>(image));
	glGenerateMipmap(GL_TEXTURE_2D);

	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);

	_textures[filename] = texture;
	return texture;
}

void Texture::SetChannelFormat(GLint format)
{
	switch (format)
	{
	case GL_RGB:
	case GL_RGBA:
		_format = format;
		break;
	default:
		cout << "Unsupported pixel format. Defaulting to RGBA" << endl;
	}
}