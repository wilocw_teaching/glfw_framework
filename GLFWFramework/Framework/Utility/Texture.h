#pragma once

#include <GL/glew.h>

#include <map>
#include <string>

class Texture
{
public:
	Texture();
	~Texture();

	GLuint GetTexture(std::string filename);
	void SetChannelFormat(GLint format);
private:
	std::map<std::string, GLuint> _textures;
	GLint _format;
};